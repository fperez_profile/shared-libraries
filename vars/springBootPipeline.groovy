def call() {

    timestamps {
        pipeline {
            agent any

            tools {
                maven "Maven 3.6.1"
            }

            options {
                buildDiscarder(logRotator(numToKeepStr: '5'))
            }

            stages {
                stage('Maven compilation') {
                    steps {                        
                        echo "Packaging project..."
                        sh "mvn clean -U package"
                        echo "Packaging project done."
                    }
                }
                stage('Run app') {
                    when {
                        anyOf {
                            branch 'develop'
                        }
                    }
                    steps {
                        withEnv(['JENKINS_NODE_COOKIE=dontkill']) {
                            sh "./runApp.sh"
                        }
                    }
                    
                }
            }
            post {
                always {
                    echo "Pipeline complete."
                }
            }
        }
    }
}